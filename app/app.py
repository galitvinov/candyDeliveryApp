#!venv/bin/activate
from flask import Flask, jsonify, request
from flask_sqlalchemy import SQLAlchemy
from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand
import datetime

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret_key'
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://root:toor@db/candyApi'
# app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://root:toor@localhost:3310/candyApi'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app, session_options={'autocommit': True})
migrate = Migrate(app, db)

manager = Manager(app)
manager.add_command('db', MigrateCommand)

#from models import Couriers, CourierHours, CourierRegions, Orders, OrderHours, Delivery
import models


@app.route('/')
def hello():
    f = open('README.txt')
    text = f.read()
    f.close()
    return text
    
    
@app.route('/couriers', methods=['POST'])
def set_couriers():
    error_id = list()
    success_id = list()
    db.session.begin()
    for part in request.json['data']:
        courier = models.Couriers()
        if courier.validate(part):
            success_id.append({'id': int(part['courier_id'])})
        else:
            error_id.append({'id': int(part['courier_id'])})
            continue
        courier.courier_id = part['courier_id']
        courier.courier_type = part['courier_type']
        db.session.add(courier)
        for region_value in part['regions']:
            region = models.CourierRegions(courier.courier_id, region_value)
            db.session.add(region)
        for hours_value in part['working_hours']:
            hours = models.CourierHours(courier.courier_id, *hours_value.split('-'))
            db.session.add(hours)
    if len(error_id) == 0 and len(success_id) > 0:
        db.session.commit()
    else:
        db.session.rollback()

    if len(error_id) or len(success_id) == 0:
        return jsonify({'validation_error': {'couriers': error_id}}), 400
    else:
        return jsonify({'couriers': success_id}), 201


@app.route('/couriers/<courier_id>', methods=['PATCH'])
def patch_couriers(courier_id):
    patch_data = request.json
    db.session.begin()
    courier = models.Couriers.query.get(courier_id)
    if not courier:
        return '', 400
    if courier.validate(patch_data, 'update'):
        if 'courier_type' in patch_data:
            courier.courier_type = patch_data['courier_type']
        if 'regions' in patch_data:
            # models.CourierRegions.query.filter(models.CourierRegions.ccouriers == int(courier_id)).delete()
            db.session.execute('DELETE FROM courier_regions WHERE courier_regions.ccouriers = '+str(courier_id))
            for region_value in patch_data['regions']:
                region = models.CourierRegions(int(courier_id), region_value)
                db.session.add(region)
        if 'working_hours' in patch_data:
            # models.CourierHours.query.filter(models.CourierHours.ccouriers == int(courier_id)).delete()
            db.session.execute('DELETE FROM courier_hours WHERE courier_hours.ccouriers = '+str(courier_id))
            for hours_value in patch_data['working_hours']:
                hours = models.CourierHours(courier.courier_id, *hours_value.split('-'))
                db.session.add(hours)
        db.session.commit()
        return jsonify(courier.object()), 200
    else:
        return '', 400


@app.route('/orders', methods=['POST'])
def set_orders():
    error_id = list()
    success_id = list()
    db.session.begin()
    for part in request.json['data']:
        order = models.Orders()
        if order.validate(part):
            success_id.append({'id': int(part['order_id'])})
        else:
            error_id.append({'id': int(part['order_id'])})
            continue
        order = models.Orders()
        order.order_id = part['order_id']
        order.weight = part['weight']
        order.region = part['region']
        db.session.add(order)
        for hours_value in part['delivery_hours']:
            hours = models.OrderHours(order.order_id, *hours_value.split('-'))
            db.session.add(hours)
    if len(error_id) == 0 and len(success_id) > 0:
        db.session.commit()
    else:
        db.session.rollback()

    if len(error_id) or len(success_id) == 0:
        return jsonify({'validation_error': {'orders': error_id}}), 400
    else:
        return jsonify({'orders': success_id}), 201


@app.route('/orders/assign', methods=['POST'])
def assign_orders():
    courier_id = request.json['courier_id']
    courier = models.Couriers.query.get(courier_id)
    if not courier:
        return '', 400
    lifting_capacity = {'foot': 10, 'bike': 15, 'car': 50}
    max_weight_courier = lifting_capacity.get(courier.courier_type, 0)
    state, list_orders, assign_time = courier.get_my_orders(max_weight_courier)
    if state == 'new':
        assign_time = datetime.datetime.now().isoformat()
        db.session.begin()
        for order_id in list_orders:
            order = models.Orders.query.get(order_id['id'])
            delivery = models.Delivery(courier.courier_id, order.order_id, assign_time)
            db.session.add(delivery)
        db.session.commit()
    return jsonify({'orders': list_orders, 'assign_time': assign_time[:-4]+"Z"}), 200


@app.route('/orders/complete', methods=['POST'])
def complete_orders():
    data = request.json
    if not models.Delivery.query.filter_by(ccouriers=data['courier_id'], corders=data['order_id']).count():
        return '', 400
    delivery = models.Delivery.query.filter_by(ccouriers=data['courier_id'], corders=data['order_id']).first()
    # complete_time = datetime.datetime.now().isoformat()
    complete_time = datetime.datetime.fromisoformat(data['complete_time'].replace('Z', '0000'))
    db.session.begin()
    db.session.execute('UPDATE delivery SET complete_time = "' + complete_time.isoformat() + '" WHERE id = ' + str(delivery.id))
    # delivery = models.Delivery.query.get(delivery.id)
    # delivery.complete_time = complete_time
    db.session.commit()
    return jsonify({'orders': delivery.corders}), 200


@app.route('/couriers/<courier_id>', methods=['GET'])
def get_courier(courier_id):
    db.session.begin()
    courier = models.Couriers.query.get(courier_id)
    if not courier:
        return '', 400
    return courier.object_with_params()

if __name__ == '__main__':
    app.run(host="0.0.0.0", port="8080")
    # app.run(host="0.0.0.0", port="8080", debug=True)

