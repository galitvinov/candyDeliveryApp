from app import db
import datetime


class Couriers(db.Model):
    __tablename__ = 'couriers'
    courier_id = db.Column(db.Integer, primary_key=True, autoincrement=False)
    courier_type = db.Column(db.String(100))

    regions = db.relationship('CourierRegions', backref='couriers')
    hours = db.relationship('CourierHours', backref='couriers')
    delivery = db.relationship('Delivery', backref='couriers')

    def __repr__(self):
        return '<Couriers {}>'.format(self.courier_id)

    def object(self):
        return_object = {'courier_id': int(self.courier_id), 'courier_type': self.courier_type, 'regions': [],
                         'working_hours': []}
        for region in CourierRegions.query.filter(CourierRegions.ccouriers == self.courier_id).all():
            return_object["regions"].append(region.region_value)
        for working_hours in CourierHours.query.filter(CourierHours.ccouriers == self.courier_id).all():
            return_object["working_hours"].append(repr(working_hours))
        return return_object

    def validate(self, insert_data, mode='insert'):
        result = True
        count_parametrs = 0
        parametrs = ['courier_id', 'courier_type', 'regions', 'working_hours']
        for field, value in insert_data.items():
            if field in parametrs:
                count_parametrs += 1
            if field not in parametrs:
                result = False
        if not result:
            return result
        if mode == 'insert' and count_parametrs == len(parametrs):
            if Couriers.query.filter(Couriers.courier_id == insert_data['courier_id']).count():
                result = False
            else:
                result = True
        if mode == 'update' and count_parametrs > 0:
            result = True
        return result

    def get_my_orders(self, max_weight_courier):
        list_orders = list()
        current_orders = Delivery.query.filter(Delivery.ccouriers == self.courier_id).all()
        for delivery in current_orders:
            list_orders.append({'id': int(delivery.corders)})
            assign_time = delivery.assign_time.isoformat()+".000000"
        if len(list_orders):
            return ['exist', list_orders, assign_time]

        data = db.session.execute('SELECT order_id id, weight, SUM( ROUND(tt2.weight, 3)) \
OVER (PARTITION BY tt2.courier_id ORDER BY tt2.weight DESC \
ROWS BETWEEN CURRENT ROW AND UNBOUNDED FOLLOWING) weight_summ \
FROM ( SELECT DISTINCT \
c.courier_id ,o.order_id, o.weight \
FROM couriers c \
LEFT JOIN courier_regions cr ON c.courier_id = cr.ccouriers \
LEFT JOIN courier_hours ch on c.courier_id = ch.ccouriers \
LEFT JOIN order_hours oh ON oh.begin_time BETWEEN ch.begin_time AND ch.end_time OR oh.end_time BETWEEN ch.begin_time AND ch.end_time \
INNER join orders o on oh.corders = o.order_id AND o.region = cr.region_value \
WHERE c.courier_id = ' + str(self.courier_id) + ') tt2 ORDER BY order_id').all()
        for order_id in data:
            if order_id['weight_summ'] <= max_weight_courier:
                list_orders.append({'id': int(order_id['id'])})
        return ['new', list_orders, datetime.datetime.now().isoformat()]

    def object_with_params(self):
        rating = 0
        earnings = db.session.execute('SELECT 500*COUNT(*) FROM delivery \
         WHERE ccouriers = ' + str(self.courier_id) + ' AND complete_time is not null').scalar()
        coefficient = {'foot': 2, 'bike': 5, 'car': 9}
        earnings = coefficient.get(self.courier_type, 0) * earnings
        if earnings:
            t = db.session.execute('SELECT CONVERT(AVG(t2.t), FLOAT) avg_time FROM ( \
            SELECT TIMESTAMPDIFF(SECOND, LAG(complete_time, 1, assign_time) \
             over (ORDER BY assign_time), complete_time) t, region \
            FROM delivery LEFT JOIN orders o on o.order_id = delivery.corders \
            WHERE ccouriers = ' + str(self.courier_id) + ' AND complete_time is not null) t2 \
             GROUP BY region ORDER BY AVG(t2.t) LIMIT 1').scalar()
            rating = (60 * 60 - min(t, 60 * 60)) / (60 * 60) * 5
            return_object = {'courier_id': int(self.courier_id), 'courier_type': self.courier_type, 'regions': [],
                             'working_hours': [], 'rating': round(rating, 2), 'earnings': int(earnings)}
        else:
            return_object = {'courier_id': int(self.courier_id), 'courier_type': self.courier_type, 'regions': [],
                             'working_hours': []}
        for region in CourierRegions.query.filter(CourierRegions.ccouriers == self.courier_id).all():
            return_object["regions"].append(region.region_value)
        for working_hours in CourierHours.query.filter(CourierHours.ccouriers == self.courier_id).all():
            return_object["working_hours"].append(repr(working_hours))
        return return_object


class CourierRegions(db.Model):
    __tablename__ = 'courier_regions'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    ccouriers = db.Column(db.Integer, db.ForeignKey('couriers.courier_id'))
    region_value = db.Column(db.Integer)

    def __repr__(self):
        return self.region_value

    def __init__(self, courier_id, region):
        self.ccouriers = courier_id
        self.region_value = region


class CourierHours(db.Model):
    __tablename__ = 'courier_hours'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    ccouriers = db.Column(db.Integer, db.ForeignKey('couriers.courier_id'))
    begin_time = db.Column(db.Time)
    end_time = db.Column(db.Time)

    def __repr__(self):
        return '{}-{}'.format(self.begin_time.strftime("%H:%M"), self.end_time.strftime("%H:%M"))

    def __init__(self, courier_id, begin, end):
        self.ccouriers = courier_id
        self.begin_time = begin
        self.end_time = end


class Orders(db.Model):
    __tablename__ = 'orders'
    order_id = db.Column(db.Integer, primary_key=True, autoincrement=False)
    weight = db.Column(db.Float)
    region = db.Column(db.Integer)

    hours = db.relationship('OrderHours', backref='orders')
    delivery = db.relationship('Delivery', backref='orders')

    def __repr__(self):
        return '<Orders {}, {}>'.format(self.weight, self.region)

    def validate(self, insert_data, mode='insert'):
        result = True
        count_parametrs = 0
        parametrs = ['order_id', 'weight', 'region', 'delivery_hours']
        for field, value in insert_data.items():
            if field in parametrs:
                count_parametrs += 1
            if field not in parametrs:
                result = False
        if not result:
            return result
        if mode == 'insert' and count_parametrs == len(parametrs):
            if Orders.query.filter(Orders.order_id == insert_data['order_id']).count():
                result = False
            else:
                result = True
        if mode == 'update' and count_parametrs > 0:
            result = True
        return result


class OrderHours(db.Model):
    __tablename__ = 'order_hours'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    corders = db.Column(db.Integer, db.ForeignKey('orders.order_id'))
    begin_time = db.Column(db.Time)
    end_time = db.Column(db.Time)

    def __repr__(self):
        return '{}-{}'.format(self.begin_time.strftime("%H:%M"), self.end_time.strftime("%H:%M"))

    def __init__(self, order_id, begin, end):
        self.corders = order_id
        self.begin_time = begin
        self.end_time = end


class Delivery(db.Model):
    __tablename__ = 'delivery'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    ccouriers = db.Column(db.Integer, db.ForeignKey('couriers.courier_id'))
    corders = db.Column(db.Integer, db.ForeignKey('orders.order_id'))
    assign_time = db.Column(db.DateTime)
    complete_time = db.Column(db.DateTime)

    def __repr__(self):
        return '<OrderHours {}, {}>'.format(self.weight, self.region)

    def __init__(self, ccouriers, corders, assign_time):
        self.ccouriers = ccouriers
        self.corders = corders
        self.assign_time = assign_time
        self.complete_time = None
