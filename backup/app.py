#!venv/bin/activate
from flask import Flask, jsonify, render_template, request
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret_key'
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://root:toor@db/candyApi'
# app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://root:toor@localhost:3310/candyApi'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)
migrate = Migrate(app, db)

#from models import Couriers, CourierHours, CourierRegions, Orders, OrderHours, Delivery
import models


@app.route('/')
def hello():
    f = open('requirements.txt')
    text = f.read()
    f.close()
    return text


@app.route('/test', methods=['GET'])
def get_tasks():
    return jsonify({'test_data': ['123', 23]})
    
    
@app.route('/couriers', methods=['POST'])
def set_couriers():
    error_id = list()
    success_id = list()
    db.session.begin()
    for part in request.json['data']:
        courier = models.Couriers()
        if courier.validate(part):
            success_id.append({'id': int(part['courier_id'])})
        else:
            error_id.append({'id': int(part['courier_id'])})
            continue
        courier.courier_id = part['courier_id']
        courier.courier_type = part['courier_type']
        db.session.add(courier)
        for region_value in part['regions']:
            region = models.CourierRegions()
            region.ccouriers = courier.courier_id
            region.region_value = region_value
            db.session.add(region)
        for hours_value in part['working_hours']:
            hours = models.CourierHours()
            hours.ccouriers = courier.courier_id
            hours.begin_time, hours.end_time = hours_value.split('-')
            db.session.add(hours)
    if len(error_id) == 0 and len(success_id) > 0:
        db.session.commit()
    else:
        db.session.rollback()

    if len(error_id) or len(success_id) == 0:
        return jsonify({'validation_error': {'couriers': error_id}}), 400
    else:
        return jsonify({'couriers': success_id}), 201


# @app.route('/couriers/<courier_id>', methods=['PATCH'])
# def patch_couriers(courier_id):



if __name__ == '__main__':
    app.run(host="0.0.0.0", port="8080", debug=True)

