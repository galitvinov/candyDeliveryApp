from app import db


class Couriers(db.Model):

    __tablename__ = 'couriers'
    courier_id = db.Column(db.Integer, primary_key=True, autoincrement=False)
    courier_type = db.Column(db.String(100))

    regions = db.relationship('CourierRegions', backref='couriers')
    hours = db.relationship('CourierHours', backref='couriers')
    delivery = db.relationship('Delivery', backref='couriers')

    def __repr__(self):
        return '<Couriers {}>'.format(self.courier_id)

    def validate(self, inser_data):
        result = True
        if 'courier_id' not in inser_data:
            result = False
        if 'courier_type' not in inser_data:
            result = False
        if 'regions' not in inser_data:
            result = False
        if 'working_hours' not in inser_data:
            result = False
        if not result:
            return result
        if Couriers.query.filter(Couriers.courier_id == inser_data['courier_id']).first():
            result = False

        return result

class CourierRegions(db.Model):

    __tablename__ = 'courier_regions'
    id = db.Column(db.Integer, primary_key=True)
    ccouriers = db.Column(db.Integer, db.ForeignKey('couriers.courier_id'))
    region_value = db.Column(db.Integer)

    def __repr__(self):
        return '<CourierRegions {}>'.format(self.region_value)


class CourierHours(db.Model):

    __tablename__ = 'courier_hours'
    id = db.Column(db.Integer, primary_key=True)
    ccouriers = db.Column(db.Integer, db.ForeignKey('couriers.courier_id'))
    begin_time = db.Column(db.Time)
    end_time = db.Column(db.Time)

    def __repr__(self):
        return '<CourierHours {} - {}>'.format(self.begin_time, self.end_time)


class Orders(db.Model):

    __tablename__ = 'orders'
    order_id = db.Column(db.Integer, primary_key=True, autoincrement=False)
    weight = db.Column(db.Float)
    region = db.Column(db.Integer)

    hours = db.relationship('OrderHours', backref='orders')
    delivery = db.relationship('Delivery', backref='orders')

    def __repr__(self):
        return '<Orders {}, {}>'.format(self.weight, self.region)


class OrderHours(db.Model):

    __tablename__ = 'order_hours'
    id = db.Column(db.Integer, primary_key=True)
    corders = db.Column(db.Integer, db.ForeignKey('orders.order_id'))
    begin_time = db.Column(db.Time)
    end_time = db.Column(db.Time)

    def __repr__(self):
        return '<OrderHours {}, {}>'.format(self.weight, self.region)


class Delivery(db.Model):

    __tablename__ = 'delivery'
    id = db.Column(db.Integer, primary_key=True)
    ccouriers = db.Column(db.Integer, db.ForeignKey('couriers.courier_id'))
    corders = db.Column(db.Integer, db.ForeignKey('orders.order_id'))
    assign_time = db.Column(db.DateTime)
    complete_time = db.Column(db.DateTime)

    def __repr__(self):
        return '<OrderHours {}, {}>'.format(self.weight, self.region)